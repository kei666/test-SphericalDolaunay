# test-shere_Voronoi

## 出力
コード中のarray_points_vertices_Delaunay_triangulation

numpyの配列で[[三角形], [三角形], ...]のようになっている

例）[ [ [x1, y1, z1], [x2, y2, z2], [x3, y3, z3] ], ... ]

## 注意点
py_sphere_Voronoiは球面上に一様な点群を生成するのに使っただけ

入力は半径1の球状の3次元点


## 結果を出力してみた
![image](./spherical_algorithm=stage_1.png)
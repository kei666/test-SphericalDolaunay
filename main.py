import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.colors as colors
import numpy as np
import scipy as sp
from py_sphere_Voronoi import voronoi_utility


'''
点群の準備と描画ツールの設定
'''
#乱数生成器
prng = np.random.RandomState(117)

#半径1の球上に1000点をランダムに配置
random_coordinate_array = voronoi_utility.generate_random_array_spherical_generators(1000, 1.0, prng)

fig_random_coords_unit_sphere = plt.figure()
ax = Axes3D(fig_random_coords_unit_sphere)
# ax.scatter(random_coordinate_array[:, 0], random_coordinate_array[:, 1], random_coordinate_array[:, 2],
#            edgecolors='none')
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1)

#Dolaunay
hull_instance = sp.spatial.ConvexHull(random_coordinate_array)
list_points_vertices_Delaunay_triangulation = []

for simplex in hull_instance.simplices:
    convex_hull_triangular_facet_vertex_coordinates = hull_instance.vertices[simplex]
    list_points_vertices_Delaunay_triangulation.append(convex_hull_triangular_facet_vertex_coordinates)

array_points_vertices_Delaunay_triangulation = np.array(list_points_vertices_Delaunay_triangulation)

#描画
for triangle in list_points_vertices_Delaunay_triangulation:
    random_color = colors.rgb2hex(sp.rand(3))
    polygon = Poly3DCollection([[random_coordinate_array[triangle[0]],
                                 random_coordinate_array[triangle[1]],
                                 random_coordinate_array[triangle[2]]]], alpha=1.0)
    polygon.set_color(random_color)
    ax.add_collection3d(polygon)


fig_random_coords_unit_sphere.set_size_inches(5, 5)
fig_random_coords_unit_sphere.savefig('spherical_algorithm=stage_1.png', dpi=300)

plt.show()